#!/usr/bin/env python
# -*- coding: utf-8 -*- #
import datetime

AUTHOR = 'Jared Contrascere'
SITENAME = 'The Contra Code'
SITEURL = 'https://blog.contracode.com'

# Flex theme-specific settings.
SITENAME = "The Contra Code"
SITELOGO = '/images/template/sidebar/logo.png'
FAVICON = '/images/favicon.ico'
SITETITLE = 'Jared Contrascere'
SITESUBTITLE = 'IT, Cloud, & Data Analytics Consultant</p>'
SITEDESCRIPTION = 'programming, python, cloud, machine learning, deep learning'

ROBOTS = 'index, follow'

CC_LICENSE = {
    'name': 'Creative Commons Attribution-NonCommercial-ShareAlike',
    'version': '4.0',
    'slug': 'by-nc-sa'
}

PATH = 'content'
OUTPUT_PATH = 'public'

ARTICLE_PATHS = ['articles']
PATH_METADATA= 'articles/(?P<subcategory_path>.*)/.*'

# Publish using 'clean' URLs.
ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html'
PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = 'tag/{slug}/index.html'
AUTHOR_URL = 'author/{slug}/'
AUTHOR_SAVE_AS = 'author/{slug}/index.html'

THEME = 'themes/flex'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing.
#FEED_ALL_ATOM = 'feeds/all.atom.xml'
#CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)
LINKS = []

# Social widget.
SOCIAL = [('linkedin', 'https://linkedin.com/in/contracode'),
          ('mastodon', 'https://fosstodon.org/@contracode'),
          ('x-twitter', 'https://x.com/contracode'),
          ('github', 'https://github.com/contracode'),
          ('gitlab', 'https://gitlab.com/contracode'),
          ('stack-exchange', 'https://webapps.stackexchange.com/users/97538/contracode'),
          ('twitch', 'https://twitch.com/contracode'),
          ('rss', '//blog.contracode.com/feeds/all.atom.xml')]

MAIN_MENU = True
MENUITEMS = (('Categories', '/categories.html'),
             ('Tags', '/tags.html'),
             ('Archives', '/archives.html'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Static paths will be copied without parsing their contents.
STATIC_PATHS = ['downloads', 'extra', 'files', 'figures', 'images', 'videos']
EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'static/custom.css'},
    'extra/custom.js': {'path': 'static/custom.js'},
}

CUSTOM_CSS = 'static/custom.css'
CUSTOM_JS = 'static/custom.js'

# Configure plugins.
MARKUP = ['md']
PLUGIN_PATHS = ['plugins']
PLUGINS = [
    'ipynb.liquid',
    'liquid_tags.img',  # embedding images
    'liquid_tags.video',  # embedding videos
    'liquid_tags.include_code',  # including code blocks
    'liquid_tags.literal',
    'post_stats',
    'subcategory',
    'summary'
]
ENABLE_MATHJAX = True
IGNORE_FILES = ['.ipynb_checkpoints']

# for liquid tags
CODE_DIR = 'downloads/code'
NOTEBOOK_DIR = 'downloads/notebooks'

# Dynamically calculate the current copyright year
copyright_year_start = 2018
copyright_year_end = datetime.date.today().year
if copyright_year_end == copyright_year_start:
    COPYRIGHT_YEAR = copyright_year_start
else:
    COPYRIGHT_YEAR = '{}-{}'.format(copyright_year_start, copyright_year_end)
