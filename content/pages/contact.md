Title: Contact
Date: 2018-06-18 19:49
Modified: 2018-06-18 19:49
Slug: contact

If you're interested in my posts, or you just have a question or comment, please feel free to contact me.

If you want to contact about typos, grammar and other errors in this blog, you can open an issue [here](https://gitlab.com/contracode/contracode.gitlab.io/issues). Otherwise you can contact me at `jcontra [at] REMOVEME gmail [dot] com`.
