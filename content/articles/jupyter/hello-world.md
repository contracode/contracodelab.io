title: Jupyter Notebook Post Template
slug: jupyter-notebook-post-template
tags: templates, jupyter notebook
authors: John Doe
date: 2018-06-21 00:00
modified:  2018-06-21 00:00
status: hidden
comments: true
type: notebook

{% notebook downloads/jupyter/hello-world.ipynb cells[0:] %}
