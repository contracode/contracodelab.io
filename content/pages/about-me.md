Title: About Me
Date: 2018-06-18 19:41
Modified: 2018-06-18 19:41
Slug: about-me

### Who I am

I'm Jared Contrascere and I live in the Washington, DC metro area.

### What I do

I work for a large IT consultancy, and I spend my time there solving hard problems for enterprise clients.

### About this blog

The goal of this blog is to talk about things I'm interested in and to share my knowledge with others.